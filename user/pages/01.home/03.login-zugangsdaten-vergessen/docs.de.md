---
title: 'Login bzw. Zugangsdaten vergessen'
menu: 'Login / Zugangsdaten vergessen'
---

## Login
1. auf Startseite **Login** wählen
2. Nutzername und Passwort eingeben
3. nach dem Login wird man zu seinem persönlichen internen Bereich weitergeleitet
![Login Formular](Login.png)

---

## Zugangsdaten vergessen?
1. auf Startseite **Login** wählen
2. weiter mit **Zugangsdaten vergessen**. Dort gibt es die Möglichkeit, sich ein neues Passwort generieren bzw. euren Benutzernamen an eure eMail zuschicken zu lassen
