---
title: Support
---

Wir sind bemüht, das Meldeportal bei fortlaufender Entwicklung trotzdem fehlerfrei zu halten. Wir bitten euch **Anregungen, Wünsche, Fehler oder Probleme** im Bezug auf des Meldeportal direkt über die Kommentar-Funktion (erscheint im Login-Bereich oben neben Logout) zu senden. Gerne auch per eMail an [omanager@orientierungslauf.de](mailto:omanager@orientierungslauf.de).

_Vielen Dank!_
euer Entwicklerteam