---
title: 'Login beantragen / erstellen'
---

Als Gast kann man lediglich die allgemeinen Informationen der eingetragene Veranstaltungen betrachten. Dafür gibt es die normale Tabellenansicht, einen Kalender sowie eine Kartenansicht.
!!! Um alle Funktionen nutzen zu können, muss man zunächst ein **Login beantragen bzw. erstellen** und in den internen Bereich wechseln.

---

## Login beantragen
1. Auf Startseite als Gast **Login beantragen** wählen

2. Verein wählen bzw. **Neuer Verein** auswählen, falls dieser nicht in Auswahlliste erscheint   
   Über **neuer Nutzer ohne Verein** ist es möglich sich als neuer Nutzer auch zunächst **vereinslos** zu registrieren.

3. Vereinsmitglied wählen bzw. **neues Mitglied** auswählen, falls Mitglied in Auswahlliste nicht erscheint.
   Für alle **OLer mit einer echten Vereinsmitgliedschaft**, welche aber unter **wähle deinen Verein** in der Vereinsmitgliederliste nicht aufgeführt sind, können über **neues Mitglied** einen Nutzer erstellen und gleichzeitig die Vereinsmitgliedschaft des ausgewählten Vereins beantragen.

   Gibt es in dem Verein bereits einen Vereinsadmin, muss dieser die Mitgliedschaft erst bestätigten. Gibt es keinen Vereinsadmin, wird dies durch die Superadmins durchgeführt.
   Dadurch sollen sogenannte Dubletten von Nutzern vermieden und nur tatsächliche Vereinsmitgliedschaften im Meldeportal aktzeptiert werden.
   Bei der Registrierung ist es möglich, den Vereinsadmin eine Nachricht zu schreiben.

4. Nach Eingabe aller Pflichtfelder ( eMail, Nutzername, Passwort ) wird eine eMail zur Aktivierung an die angegebene Adresse gesendet.
Die eMail enthält einen Link zu einem persönlichen Aktivierungsbereich. Mit Bestätigung von Nutzername und Passwort wird dein Login freigeschalten.
! Die Aktivierung muss innerhalb von 24h erfolgen. Danach wird der Link deaktiviert und das Login muss neu beantragt werden