---
title: 'Mithilfe erwünscht'
---

Im gesamten internen Bereich erscheint in der obersten kleinen Menü-Zeile der Eintrag **Kommentar schreiben**. Damit ist es als Nutzer möglich, einen Kommentar direkt an uns Entwickler zu schicken.

1. an einer Stelle/ auf der Seite im Meldeportal **Kommentar schreiben** auswählen, zu der etwas an uns Entwickler geschrieben werden soll
2. Kommentar schreiben und Betreff auswählen (Erweiterungsanforderung,Hinweis,Fehler) und absenden
3. wir sehen den Kommentar und entsprechende Stelle/Seite von OManager, von welcher dieser geschrieben wurde! Damit ist euer Kommentar für uns besser zuzuordnen.
4. unter **Meine Kommentare** werden alle abgegebenen Kommentare aufgelistet. Dort sind auch unsere konkreten Antworten zu finden.

!!!! Eine effektive Entwicklung des Meldeportals hängt hauptsächlich von eurem **Feedback** ab!
Habt ihr ein generelles Thema, kann dieses gerne auch an omanager@orientierungslauf.de geschrieben werden.

_Vielen Dank!_
euer Entwicklerteam
