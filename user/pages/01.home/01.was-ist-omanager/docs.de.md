---
title: 'Was ist O-Manager?'
date: '08-10-2016 22:37'
---

O-Manager ist ein zentrales **Online-Meldesystem für den Orientierungslauf in Deutschland**. Das übergeordnete Ziel ist, ein System zu entwickeln und zu etablieren, dass in Zukunft als Meldeportal für alle Arten von OL-Veranstaltungen genutzt werden kann. Es soll v.a. dem Veranstalter dabei helfen Meldungen einheitlich zu erfassen.
!!!! Das Projekt wird offiziell vom TK's unterstützt und erhielt 2016 den Wanderpokal des [Förderverein Orientierungslauf e.V.](http://fv.orientierungslauf.de/foerderung.php)

---


## Warum ein zentrales Meldeportal?
In Deutschland werden Meldungen für Orientierungsläufe sehr unterschiedlich von Seiten der Teilnehmer bzw. Vereine an den Ausrichter getragen. Gerade für kleinere Vereine oder größere Veranstaltungen ist das enorm zeitaufwendig. In den letzten Jahren werden bereits exsisterende internationale Meldesysteme zunehmend durch die Ausrichter genutzt. Dadurch hat sich für den Ausrichter die Meldungsverwaltung erheblich vereinfacht.

Durch ein eigenes zentrales Meldesystem in Deutschland können die speziellen Anforderungen an Meldungen (z.B. Thema Startpass) im System implementiert werden. Ziel ist eine umfassende Unterstützung für die Organisatoren. Zudem sind durch die Vergabe von Deutschen-OL-IDs (DOID) für OLer durch das Meldeportal auch perspektivisch die Berechnungen von Ranglisten möglich.

---

## Wie ist das Projekt entstanden?
Das TK des deutschen OLs hat 2013 eine Gruppe von Interessierten Olern ins Leben gerufen. Diese Gruppe hat exsitierende Meldesystem umfrangreich analysiert sowie Anforderungen an eigenes zusammen zu tragen. Daraus resultierend began 2014 die Entwicklung.

---

## Technologie
* **Datenbank**
  [Oracle Database 11g XE](http://www.oracle.com/technetwork/database/database-technologies/express-edition/overview/index.html) _(kostenlose Version)_
* **Hosting**
  durch [Robotron Datenbank-Software GmbH](https://www.robotron.de) _(kostenlose Bereitstellung)_
* **Entwicklungsumgebung**
  [Oracle Application Express (APEX)](http://www.oracle.com/technetwork/developer-tools/apex/overview/index-155186.html) _(kostenfrei)_

---

### Wer sind die Entwickler?
 * **Björn Heinemann** (SV Robotron Dresden,Geschäftsführer _Robotron Datenbank-Software GmbH_)
 * **Janek Leibiger** (Post SV Dresden)